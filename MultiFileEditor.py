# TODO:
# Requirement(include_list, exclude_list)
# regex_flags z konstruktora do metody
# dodac numery linii obok liczby porzadkowej
# thread for os.walk, processes for regex crunching, thread for printing
# 74: path_include == file_without_ext, ~ = full_file_path

"""
Multitool for searching files through folders, checking their contents, finding/replacing contents using regexes
"""

import os
import re
import time

from tqdm import tqdm


class MultiFileEditor(object):
    """
    Multitool for searching files through folders, checking their contents, finding/replacing contents using regexes
    """

    def __init__(self, regex_flags=re.IGNORECASE):
        """
        :param regex_flags: for example re.IGNORECASE | re.DOTALL | re.VERBOSE | re.DEBUG
        """
        assert isinstance(regex_flags, int)
        self._regex_flags = regex_flags
        self._directory = ''
        self._matching_files = 0
        self._regex_matches = 0
        self._not_opened_files = 0
        self._output_list = list()

    def _string_meet_regex_conditions(self, contents, text_include, text_exclude):
        """
        Check if string meet requirements
        :param contents: string to be checked
        :param text_include: regex pattern (or list/tuple of patterns) that must be found
        :param text_exclude: regex pattern (or list/tuple of patterns) that mustn't be found
        :return: True if all requirements are meet, False otherwise
        """
        assert isinstance(contents, str)
        assert isinstance(text_include, (tuple, list, str))
        assert isinstance(text_exclude, (tuple, list, str))

        if isinstance(text_include, (tuple, list)):
            for single_text_include in text_include:
                if len(re.findall(single_text_include, contents, self._regex_flags)) == 0:
                    return False
        elif isinstance(text_include, str):
            if len(re.findall(text_include, contents, self._regex_flags)) == 0:
                return False

        if isinstance(text_exclude, (tuple, list)):
            for single_text_exclude in text_exclude:
                if len(re.findall(single_text_exclude, contents, self._regex_flags)) > 0:
                    return False
        elif isinstance(text_exclude, str):
            if len(re.findall(text_exclude, contents, self._regex_flags)) > 0:
                return False

        return True

    def _get_file_list(self, directory, extension, path_include, path_exclude):
        """
        Returning file paths using filters
        :param directory: directory where searching starts recursively
        :param extension: white list of file extensions
        :param path_include: regex pattern (or list/tuple of patterns) that must be found
        :param path_exclude: regex pattern (or list/tuple of patterns) that mustn't be found
        :return: list of file paths (absolute)
        """
        assert isinstance(directory, str)
        assert isinstance(extension, (tuple, str))

        list_to_return = []
        for root, _, files in os.walk(os.path.abspath(directory)):
            for filename in files:
                if filename.lower().endswith(extension):
                    full_file_path = os.path.join(root, filename)
                    if self._string_meet_regex_conditions(full_file_path, path_include, path_exclude):
                        list_to_return.append(full_file_path)
        return list_to_return

    def _do_magic_with_one_file(self, file_path, text_include, text_exclude, find_pattern, regex_replace):
        """
        :param file_path: absolute path to file which needs to be checked
        :param text_include: regex pattern (or list/tuple of patterns) that must be found inside each file
        :param text_exclude: regex pattern (or list/tuple of patterns) that mustn't be found inside each file
        :param find_pattern: regex pattern to find in each file
        :param regex_replace: tuple/list of regex replace pairs; can be ('a', 'b') or ( ('a','b'), ('c','d') )
        :return: nothing
        """
        assert isinstance(file_path, str)
        assert isinstance(text_include, (tuple, list, str))
        assert isinstance(text_exclude, (tuple, list, str))
        assert isinstance(find_pattern, str)
        assert isinstance(regex_replace, (tuple, list))

        # read file contents
        with open(file_path, mode='rt', newline='', encoding='utf-8') as opened_file:
            try:
                file_contents = opened_file.read()
            except UnicodeDecodeError as error:
                self._not_opened_files += 1
                return
            except MemoryError as error:
                self._not_opened_files += 1
                return
            except PermissionError as error:
                self._not_opened_files += 1
                return

        # check file contents
        if not self._string_meet_regex_conditions(file_contents, text_include, text_exclude):
            return

        # find pattern
        if find_pattern:
            matched_objects = re.findall(find_pattern, file_contents, self._regex_flags)
            # strip whitespaces
            matched_objects = list(map(str.strip, matched_objects))
        else:
            matched_objects = []

        # update statistics data
        if text_include != ():  # if there is anything to filter by contents (and we are after check)
            self._matching_files += 1
        elif text_include == () and len(matched_objects) > 0:  # if nothing to filter by contents but found sth to print
            self._matching_files += 1

        self._regex_matches += len(matched_objects)
        self._output_list.append((file_path, matched_objects))

        # replace file contents
        if regex_replace:
            # handle multiple replaces -> (('1','2'), ('3','4'), ('5','6'))
            if all(isinstance(item, list) or isinstance(item, tuple) for item in regex_replace):
                for single_replace in regex_replace:
                    assert len(single_replace) == 2, 'supported replace form is (x,y) or ((x,y),(a,b))'
                    assert isinstance(single_replace[0], str)
                    assert isinstance(single_replace[1], str) or callable(regex_replace[1])
                    file_contents = re.sub(single_replace[0], single_replace[1], file_contents)
            else:  # handle single replace -> ('a', 'b')
                assert len(regex_replace) == 2, 'supported replace form is (x,y) or ((x,y),(a,b))'
                assert isinstance(regex_replace[0], str)
                assert isinstance(regex_replace[1], str) or callable(regex_replace[1])
                file_contents = re.sub(regex_replace[0], regex_replace[1], file_contents)

            # save modified contents to file
            with open(file_path, mode='wt', newline='') as opened_file:
                opened_file.write(file_contents)

    @staticmethod
    def _get_found_elements_for_printing(file_path, found_regexes):
        """
        get pretty printed file path and regex matches from this file
        :param file_path: file path to use
        :param found_regexes: list of regex matches
        :return: formatted string
        """
        file_path = file_path.replace('\\', '/')  # use human friendly separators

        output = ''
        if len(found_regexes) > 0:
            output += f'\n{file_path}:\n'

            for count, found_regex in enumerate(found_regexes, 1):
                output += f'{count}: {found_regex}\n'
        return output

    def multi_file_editor(
            self,
            directory,
            file_extension='',
            path_include=(), path_exclude=(),
            text_include=(), text_exclude=(),
            find_pattern=r'',
            regex_replace=(),
            print_progress=True,
            print_results=True):
        """
        Multitool for searching files through folders, checking their contents, printing/replacing contents using regex
        :param directory: directory where searching starts recursively
        :param file_extension: white list of file extensions
        :param path_include: list of strings that must be inside path
        :param path_exclude: list of strings that mustn't be inside path
        :param text_include: regex pattern (or list/tuple of patterns) that must be found inside each file
        :param text_exclude: regex pattern (or list/tuple of patterns) that mustn't be found inside each file
        :param find_pattern: regex pattern to find and return in each file
        :param regex_replace: tuple/list of regex replace pairs; can be ('a', 'b') or ( ('a','b'), ('c','d') )
        :param print_progress: show progress (0-100%, matched files, matched regexes)
        :param print_results: pretty print (file1, [found1,found2]) etc on screen
        :return: ((filepath1, [found1,found2]), (filepath2, [found1])...
        """
        time_at_program_start = time.time()
        self._directory = directory
        file_list = self._get_file_list(directory, file_extension, path_include, path_exclude)
        file_list_len = len(file_list)

        if file_list_len == 0:
            if print_progress:
                print('No files with given extension found in specified directory')
        else:  # file_list_len > 0
            if print_progress:  # printing progress -> tqdm must handle printing
                tqdm_iterable = tqdm(file_list, unit='files')
                last_output_list_len = len(self._output_list)
                for file_path in tqdm_iterable:
                    self._do_magic_with_one_file(file_path, text_include, text_exclude, find_pattern, regex_replace)

                    is_something_new_in_output_list = last_output_list_len != len(self._output_list)
                    if print_results and is_something_new_in_output_list:
                        file_path2, found_list = self._output_list[-1]
                        assert file_path == file_path2
                        tqdm_iterable.write(self._get_found_elements_for_printing(file_path, found_list), end='')

                    tqdm_iterable.set_postfix(files=self._matching_files, regexes=self._regex_matches,
                                              errors=self._not_opened_files)

                    last_output_list_len = len(self._output_list)
            else:  # not print_progress -> normal builtin printing
                last_output_list_len = len(self._output_list)
                for file_path in file_list:
                    self._do_magic_with_one_file(file_path, text_include, text_exclude, find_pattern, regex_replace)

                    is_something_new_in_output_list = last_output_list_len != len(self._output_list)
                    if print_results and is_something_new_in_output_list:
                        file_path2, found_list = self._output_list[-1]
                        assert file_path == file_path2
                        print(self._get_found_elements_for_printing(file_path, found_list), end='')

                    last_output_list_len = len(self._output_list)
        return self._output_list


if __name__ == '__main__':
    # rg ".+\.format.+" -i -g "*.py" --no-filename|sed 's/^\s*//g'|sort -u
    MultiFileEditor().multi_file_editor(directory='D:/Projects',
                                        file_extension='py',
                                        text_include='\.format',
                                        find_pattern='.+\.format.+',
                                        print_progress=False)
