# MultiFileEditor

MultiFileEditor is tool for searching files through folders, checking their contents, printing/replacing contents using regex

# Documentation
	def multi_file_editor(
		self,
		directory,
		file_extension='',
		path_include=(), path_exclude=(),
		text_include=(), text_exclude=(),
		find_pattern=r'',
		regex_replace=(),
		print_progress=True,
		print_results=True,
		print_report=True):
	Multitool for searching files through folders, checking their contents, printing/replacing contents using regex
	:param directory: directory where searching starts recursively
	:param file_extension: white list of file extensions
	:param path_include: list of strings that must be inside path
	:param path_exclude: list of strings that must not be inside path
	:param text_include: regex pattern (or list/tuple of patterns) that must be found inside each file
	:param text_exclude: regex pattern (or list/tuple of patterns) that must not be found inside each file
	:param find_pattern: regex pattern to find and return in each file
	:param regex_replace: tuple/list of regex replace pairs; can be ('a', 'b') or ( ('a','b'), ('c','d') )
	:param print_progress: show progress (0-100%, matched files, matched regexes)
	:param print_results: pretty print (file1, [found1,found2]) etc on screen
	:param print_report: print report with matched files/regexes count, used parameters etc
	:return: ((filepath1, [found1,found2]), (filepath2, [found1])...

# Examples
	# print files with 'anything' in *.c files inside D:
	MultiFileEditor().multi_file_editor(r'D:/', file_extension=('c'), text_include='anything')  

	# find '=>' in *.c files (text_include is used for speedup, because of heavy .*{}.* printing regex)
	MultiFileEditor().multi_file_editor(r'D:/', file_extension=('c'), text_include=text_to_find, find_pattern=r'.*{}.*'.format(text_to_find))  

	# double all numbers in *.txt files using regex replace function
	def replace_func(match):  
		return str(int(match.group(0))*2)  
	MultiFileEditor().multi_file_editor(r'D:/', file_extension=('txt'), regex_replace=(r'(\d+)', replace_func))